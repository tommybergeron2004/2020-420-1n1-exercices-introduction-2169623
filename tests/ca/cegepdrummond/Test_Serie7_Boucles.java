package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie7_Boucles extends SimulConsole {

    @Test
    @Order(1)
    void test_while1() throws Exception {
        choixMenu("7a");
        assertSortie("0", false);
        assertSortie("1", false);
        assertSortie("2", false);
        assertSortie("3", false);
        assertSortie("4", false);
        assertSortie("5", false);
        assertSortie("fin", false);

    }

    @Test
    @Order(2)
    void test_while2() throws Exception {
        choixMenu("7b");
        ecrire("5");
        ecrire("8");
        assertSortie("5", false);
        assertSortie("6", false);
        assertSortie("7", false);
        assertSortie("8", false);
        assertSortie("fin", false);

        choixMenu("7b");
        ecrire("7");
        ecrire("7");
        assertSortie("7", false);
        assertSortie("fin", false);
    }

    @Test
    @Order(3)
    void test_while3() throws Exception {
        choixMenu("7c");
        ecrire("5");
        ecrire("8");
        assertSortie("5", false);
        assertSortie("6", false);
        assertSortie("7", false);
        assertSortie("8", false);
        assertSortie("fin", false);

        choixMenu("7c");
        ecrire("9");
        ecrire("7");
        assertSortie("7", false);
        assertSortie("8", false);
        assertSortie("9", false);
        assertSortie("fin", false);

        choixMenu("7c");
        ecrire("10");
        ecrire("10");
        assertSortie("10", false);
        assertSortie("fin", false);
    }

    @Test
    @Order(4)
    void test_dowhile1() throws Exception {
        choixMenu("7d");
        ecrire("9");
        ecrire("7");
        ecrire("9");
        ecrire("7");
        ecrire("0");
        assertSortie("32", false);

        choixMenu("7d");
        ecrire("1");
        ecrire("2");
        ecrire("3");
        ecrire("4");
        ecrire("0");
        assertSortie("10", false);
    }

    @Test
    @Order(5)
    void test_dowhile2() throws Exception {
        choixMenu("7e");
        ecrire("0");
        assertSortie("0", false);


        choixMenu("7e");
        ecrire("42");
        ecrire("0");
        assertSortie("42", false);

        choixMenu("7e");
        ecrire("10");
        ecrire("3");
        ecrire("0");
        assertSortie("10", false);

        choixMenu("7e");
        ecrire("3");
        ecrire("10");
        ecrire("0");
        assertSortie("10", false);
    }

    @Test
    @Order(6)
    void test_dowhile3() throws Exception {
        choixMenu("7f");
        ecrire("1 4 3 2 4 5 0");
        assertSortie("6", false);

        choixMenu("7f");
        ecrire("0");
        assertSortie("0", false);

        choixMenu("7f");
        ecrire("1 4 3 2 4 5 0 3 3");
        assertSortie("6", false);
    }

    @Test
    @Order(7)
    void test_for1() throws Exception {
        choixMenu("7g");
        ecrire("4 7");
        assertSortie("4 5 6 7 ", false);

        choixMenu("7g");
        ecrire("5 5");
        assertSortie("5 ",false);

    }
    
    @Test
    @Order(8)
    void test_for2() throws Exception {
        choixMenu("7h");
        assertSortie("0 2 4 6 8 10 12 14 16 18 20 ", false);
        
    }

    @Test
    @Order(9)
    void test_for3() throws Exception {
        choixMenu("7i");
        assertSortie("18 15 12 9 6 3 0 ", false);

    }

    @Test
    @Order(10)
    void test_boucle1() throws Exception {
        choixMenu("7j");
        ecrire("chose");
        assertSortie("esohc", false);

        choixMenu("7j");
        ecrire("c");
        assertSortie("c", false);
    }

    @Test
    @Order(11)
    void test_boucle2() throws Exception {
        choixMenu("7k");
        ecrire("allo123");
        assertSortie("lettres: 4", false);
        assertSortie("nombres: 3", false);

        choixMenu("7k");
        ecrire("allo");
        assertSortie("lettres: 4", false);
        assertSortie("nombres: 0", false);

        choixMenu("7k");
        ecrire("123");
        assertSortie("lettres: 0", false);
        assertSortie("nombres: 3", false);

        choixMenu("7k");
        ecrire("123   ");
        assertSortie("lettres: 0", false);
        assertSortie("nombres: 3", false);

        choixMenu("7k");
        ecrire("123%?$&");
        assertSortie("lettres: 0", false);
        assertSortie("nombres: 3", false);
    }

    @Test
    @Order(12)
    void test_boucle3() throws Exception {
        choixMenu("7l");
        ecrire("0");
        assertSortie("bravo", false);

        choixMenu("7l");
        ecrire("9");
        assertSortie("bravo", false);

        choixMenu("7l");
        ecrire("allo");
        assertSortie("entrez un seul caractère", false);
        ecrire("");
        assertSortie("entrez au moins 1 caractère", false);
        ecrire("a");
        assertSortie("entrez un nombre de 0 à 9", false);
        ecrire("5");
        assertSortie("bravo", false);

    }

    @Test
    @Order(13)
    void test_boucle4() throws Exception {
        choixMenu("7m");
        ecrire("allo");
        assertSortie("Allo", false);

        choixMenu("7m");
        ecrire("allo toi");
        assertSortie("Allo Toi", false);

        choixMenu("7m");
        ecrire("a");
        assertSortie("A", false);

        choixMenu("7m");
        ecrire("a b");
        assertSortie("A B", false);

    }

    @Test
    @Order(14)
    void test_boucle5() throws Exception {
        choixMenu("7n");
        ecrire("kayak");
        assertSortie("oui", false);

        choixMenu("7n");
        ecrire("non");
        assertSortie("oui", false);

        choixMenu("7n");
        ecrire("oui");
        assertSortie("non", false);

        choixMenu("7n");
        ecrire("abccba");
        assertSortie("oui", false);
    }


}
